package esinf;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author DEI-ESINF
 */
public class TREE_WORDS extends BST<TextWord> {

    public void createTree() throws FileNotFoundException {
        Scanner ler = new Scanner(new File("src/esinf/xxx.xxx"));
        while (ler.hasNextLine()) {
            insert(new TextWord(ler.next().replace('.', ' ').replace(',', ' ').trim(), 1));
        }
        ler.close();
    }
//================================================================ 

    /**
     * Inserts a new word in the tree, or increments the number of its
     * occurrences.
     *
     * @param element
     */
    @Override
    public void insert(TextWord element) {
        root = insert(element, root);
    }

    private Node<TextWord> insert(TextWord element, Node<TextWord> node) {
        if (node == null) {
            return new Node<>(element, null, null);
        }

        if (node.getElement().getWord().equals(element.getWord())) {
            node.getElement().incOcorrences();
        } else {

            if (node.getElement().compareTo(element) > 0) {
                node.setLeft(insert(element, node.getLeft()));
            } else {
                node.setRight(insert(element, node.getRight()));
            }
        }

        return node;
    }
//****************************************************************

    /**
     * Returns a map with a list of words for each occurrence found.
     *
     * @return a map with a list of words for each occurrence found.
     */
    public Map<Integer, List<String>> getWordsOccurrences() {
        Map<Integer, List<String>> map = new HashMap<>();
        Iterable<TextWord> inOrder = inOrder();
        List<String> list = new ArrayList<>();

        //get max
        int max = Integer.MIN_VALUE;
        for (TextWord element : postOrder()) {
            if (element.getOcorrences() > max) {
                max = element.getOcorrences();
            }
        }

        for (int i = 1; i <= max; i++) {
            list.clear();
            for (TextWord element : inOrder) {
                if(element.getOcorrences()==i){
                    list.add(element.getWord());
                }
            }
            map.put(i, new ArrayList<>(list));
        }
        return map;
    }

}
