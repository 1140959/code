package esinf;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 *
 * @author DEI-ESINF
 */
public class Utils {

    public static <E extends Comparable<E>> Iterable<E> sortByBST(List<E> listUnsorted) {

        PriorityQueue<E> heap = new PriorityQueue<>(listUnsorted.size());
        List<E> sorted = new ArrayList<>(listUnsorted.size());

        for (E e : listUnsorted) {
            heap.add(e);
        }

        for (int i = 0; i < listUnsorted.size(); i++) {
            sorted.add(i, heap.remove());
        }
        
        return sorted;
    }
}
