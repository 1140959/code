package graph;

import java.util.LinkedList;

/**
 *
 * @author DEI-ESINF
 */
public class EdgeAsDoubleGraphAlgorithms {

    /**
     * Determine the shortest path to all vertices from a vertex using
     * Dijkstra's algorithm To be called by public short method
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param knownVertices previously discovered vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param minDist minimum distances in the path
     *
     */
    private static <V> void shortestPath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, boolean[] knownVertices, int[] verticesIndex, double[] minDist) {

        double infinity = Double.POSITIVE_INFINITY;

        // zerar variaveis
        for (int i = 0; i < graph.numVertices; i++) {
            minDist[i] = infinity;
            verticesIndex[i] = -1;
            knownVertices[i] = false;
        }

        minDist[sourceIdx] = 0;

        while (sourceIdx != -1) {
            V vOrigin = graph.vertices.get(sourceIdx);
            knownVertices[sourceIdx] = true;

            int j;
            for (V vAdj : graph.directConnections(vOrigin)) {

                j = graph.toIndex(vAdj);
                double weight = graph.getEdge(vAdj, vOrigin);

                if (knownVertices[j] == false && minDist[j] > minDist[sourceIdx] + weight) {
                    minDist[j] = minDist[sourceIdx] + weight;
                    verticesIndex[j] = sourceIdx;
                }
            }

            // get next vert
            double min = infinity;
            int vertIndex = -1;

            for (int i = 0; i < knownVertices.length; i++) {
                if (knownVertices[i] == false && min > minDist[i]) {
                    min = minDist[i];
                    vertIndex = i;
                }
            }

            sourceIdx = vertIndex;
        }
    }

    /**
     * Determine the shortest path between two vertices using Dijkstra's
     * algorithm
     *
     * @param graph Graph object
     * @param source Source vertex
     * @param dest Destination vertices
     * @param path Returns the vertices in the path (empty if no path)
     * @return minimum distance, -1 if vertices not in graph or no path
     *
     */
    public static <V> double shortestPath(AdjacencyMatrixGraph<V, Double> graph, V source, V dest, LinkedList<V> path) {
        path.clear();
        if (graph.toIndex(source) == -1 || graph.toIndex(dest) == -1) {
            return -1;
        }

        int size = graph.numVertices;
        boolean[] knownVertices = new boolean[size];
        int[] verticesIndex = new int[size];
        double[] minDist = new double[size];

        shortestPath(graph, graph.toIndex(source), knownVertices, verticesIndex, minDist);
        if (knownVertices[graph.toIndex(dest)] == false) {
            return -1;
        }

        recreatePath(graph, graph.toIndex(source), graph.toIndex(dest), verticesIndex, path);

        return minDist[graph.toIndex(dest)];
    }

    /**
     * Recreates the minimum path between two vertex, from the result of
     * Dikstra's algorithm
     *
     * @param graph Graph object
     * @param sourceIdx Source vertex
     * @param destIdx Destination vertices
     * @param verticesIndex index of vertices in the minimum path
     * @param Queue Vertices in the path (empty if no path)
     */
    private static <V> void recreatePath(AdjacencyMatrixGraph<V, Double> graph, int sourceIdx, int destIdx, int[] verticesIndex, LinkedList<V> path) {
        path.addFirst(graph.vertices.get(destIdx));
        if (sourceIdx != destIdx) {
            destIdx = verticesIndex[destIdx];
            recreatePath(graph, sourceIdx, destIdx, verticesIndex, path);
        }
    }

    /**
     * Creates new graph with minimum distances between all pairs uses the
     * Floyd-Warshall algorithm
     *
     * @param graph Graph object
     * @return the new graph
     */
    public static <V> AdjacencyMatrixGraph<V, Double> minDistGraph(AdjacencyMatrixGraph<V, Double> graph) {

        AdjacencyMatrixGraph<V, Double> newGraph = GraphAlgorithms.transitiveClosure(graph, Double.POSITIVE_INFINITY);
        int size = newGraph.numVertices;
        double[] minDist = new double[size];

        for (int i = 0; i < size; i++) {
            shortestPath(newGraph, i, new boolean[size], new int[size], minDist);
            for (int j = 0; j < minDist.length; j++) {
                if (minDist[j]!=Double.POSITIVE_INFINITY) {
                    newGraph.removeEdge(i, j);
                    newGraph.insertEdge(newGraph.vertices.get(i), newGraph.vertices.get(j), minDist[j]);
                }
            }
        }     
        
        return newGraph;
    }

}
