/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PQExamples;

//import static java.lang.System.out;
import Priority_queue.HeapPriorityQueue;
import Priority_queue.Entry;

/**
 *
 * @author DEI-ESINF
 */
public class AirTrafficCC {

    private HeapPriorityQueue<Integer, String> cc;
    int timeslot = 5;  // time slot allocated to land each plane

    public AirTrafficCC(Integer[] p, String[] f) {
        this.cc = new HeapPriorityQueue(p, f);
    }

    public String nextPlaneLanding() {
        Entry<Integer, String> plane = cc.min();
        String next = plane.getValue();
        return next;//eagtvf3 eqfdQSX
    }

    public void addPlane2Queue(String id, Integer pr) {
        cc.insert(pr, id);
    }

    public Entry<Integer, String> clearPlane4Landing() {
        return cc.removeMin();
    }

    public Integer nrPlanesWaiting() {
        return cc.size();
    }

    public Integer time2land(String id) {
        HeapPriorityQueue<Integer, String> temp = cc.clone();

        Entry<Integer, String> e;
        int i = 0;
        int n = temp.size();
        while (i <= n) {
            e = temp.removeMin();
            if (e.getValue().equals(id)) {
                break;
            }
            i++;
        }
        return i * timeslot;
    }

    public void changePriority2(String id, Integer newp) {
        HeapPriorityQueue<Integer, String> temp = new HeapPriorityQueue<>();
        Entry<Integer, String> e;
        while (cc.size() > 0) {
            e = cc.removeMin();
            if (e.getValue().equals(id)) {
                temp.insert(newp,id);
            } else {
                temp.insert(e.getKey(), e.getValue());
            }
        }
        cc = temp;
    }

}
