package locker;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Janela extends JFrame {

    private boolean botao;

    public Janela() {

        botao = true;
        JPanel p = new JPanel();

        p.setFocusable(true);
        p.requestFocusInWindow();
        p.add(new JLabel("Key Não Inserida!"));
        
        p.addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent e) {
                
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_P){
                System.out.println("ola");
                botao = false;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                
            }
        });
        
        this.add(p);
        this.setVisible(true);
        this.pack();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.freeze(15);
        this.dispose();
    }

    private void freeze(long segundos) {

        final long MULTIPLICADOR = 1000;

        long t = System.currentTimeMillis();
        long end = t + segundos * MULTIPLICADOR;

//        while (System.currentTimeMillis() < end) {        
        while (botao) {
            try {
                Robot robot = new Robot();
                robot.mouseMove(960, 540);

            } catch (AWTException ex) {
                Logger.getLogger(Locker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
