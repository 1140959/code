package sort;

import DP1140959.Sort;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author David Pinheiro
 * @author 1140959@isep.ipp.pt
 */
public class SortTest {
    
    public SortTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of bubbleSort method, of class Sort.
     */
    @Test
    public void testBubbleSort() {
        System.out.println("bubbleSort");
        Comparable[] values = {9,9,6,3,5,2,1};
        Comparable[] expected ={1,2,3,5,6,9,9};
        
        List<Comparable> values2 = new ArrayList<>(Arrays.asList(9,9,6,3,5,2,1));
        List<Comparable> expected2 = new ArrayList<>(Arrays.asList(1,2,3,5,6,9,9));
        
        String[] values3 = {"Joaquim","Alberto","David"};
        String[] expected3 ={"Alberto","David","Joaquim"};
        
        Sort.bubbleSort(values); 
        Sort.bubbleSort(values2);
        Sort.bubbleSort(values3);
        
        assertTrue(Arrays.equals(expected, values));
        assertTrue(Arrays.equals(expected3, values3));
        assertTrue(values2.equals(expected2));
    }

    /**
     * Test of insertionSort method, of class Sort.
     */
    @Test
    public void testInsertionSort() {
        System.out.println("insertionSort");
        Comparable[] values = {9,9,6,3,5,2,1};
        Comparable[] expected ={1,2,3,5,6,9,9};
        
        List<Comparable> values2 = new ArrayList<>(Arrays.asList(9,9,6,3,5,2,1));
        List<Comparable> expected2 = new ArrayList<>(Arrays.asList(1,2,3,5,6,9,9));
        
        String[] values3 = {"Joaquim","Alberto","David"};
        String[] expected3 ={"Alberto","David","Joaquim"};
        
        Sort.insertionSort(values); 
        Sort.insertionSort(values2); 
        Sort.insertionSort(values3); 
        
        assertTrue(Arrays.equals(expected, values));
        assertTrue(Arrays.equals(expected3, values3));
        assertTrue(values2.equals(expected2));
    }

    /**
     * Test of mergeSort method, of class Sort.
     */
    @Test
    public void testMergeSort() {
        System.out.println("mergeSort");
        Comparable[] values = {9,9,6,3,5,2,1};
        Comparable[] expected ={1,2,3,5,6,9,9};
        
        List<Comparable> values2 = new ArrayList<>(Arrays.asList(9,9,6,3,5,2,1));
        List<Comparable> expected2 = new ArrayList<>(Arrays.asList(1,2,3,5,6,9,9));
        
        String[] values3 = {"Joaquim","Alberto","David"};
        String[] expected3 ={"Alberto","David","Joaquim"};
        
        Sort.mergeSort(values); 
        Sort.mergeSort(values2); 
        Sort.mergeSort(values3); 
        
        assertTrue(Arrays.equals(expected, values));
        assertTrue(Arrays.equals(expected3, values3));
        assertTrue(values2.equals(expected2));
    }

    /**
     * Test of quickSort method, of class Sort.
     */
    @Test
    public void testQuickSort() {
        System.out.println("quickSort");
        Comparable[] values = {9,9,6,3,5,2,1};
        Comparable[] expected ={1,2,3,5,6,9,9};
        
        String[] values2 = {"Joaquim","Alberto","David"};
        String[] expected2 ={"Alberto","David","Joaquim"};
        
        List<Comparable> values3 = new ArrayList<>(Arrays.asList(9,9,6,3,5,2,1));
        List<Comparable> expected3 = new ArrayList<>(Arrays.asList(1,2,3,5,6,9,9));
        
        Sort.quickSort(values,0,values.length-1); 
        Sort.quickSort(values2,0,values2.length-1); 
        Sort.quickSort(values3,0,values3.size()-1); 
        
        assertTrue(Arrays.equals(expected, values));
        assertTrue(Arrays.equals(expected2, values2));
        assertTrue(values3.equals(expected3));
    }
    
}
