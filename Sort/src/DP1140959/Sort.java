package DP1140959;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class Sort Collection of Sorting algorithms
 *
 * @author David Pinheiro
 * @author 1140959@isep.ipp.pt
 */
public class Sort {

    /**
     * bubbleSort com arrays
     *
     * @param <T> implementa comparable
     * @param values array para sort
     */
    public static <T extends Comparable<T>> void bubbleSort(T[] values) {
        for (int i = 0; i < values.length; i++) {
            for (int j = values.length - 1; j >= 1; j--) {
                if (values[j - 1].compareTo(values[j]) > 0) {
                    T aux;
                    aux = values[j - 1];
                    values[j - 1] = values[j];
                    values[j] = aux;
                }
            }
        }
    }

    /**
     * bubbleSort com lists
     * 
     * @param <T> implementa comparable
     * @param values list para sort
     */
    public static <T extends Comparable<T>> void bubbleSort(List<T> values) {
        for (int i = 0; i < values.size(); i++) {
            for (int j = values.size() - 1; j >= 1; j--) {
                if (values.get(j - 1).compareTo(values.get(j)) > 0) {
                    T aux;
                    aux = values.get(j - 1);
                    values.set(j - 1, values.get(j));
                    values.set(j, aux);
                }
            }
        }
    }

    /**
     * insertionSort com array
     * 
     * @param <T> implementa comparable
     * @param values array para sort
     */
    public static <T extends Comparable<T>> void insertionSort(T[] values) {
        int i, j;
        T newValue;
        for (i = 1; i < values.length; i++) {
            newValue = values[i];
            j = i;
            while (j > 0 && values[j - 1].compareTo(newValue) > 0) {
                values[j] = values[j - 1];
                j--;
            }
            values[j] = newValue;
        }
    }

    /**
     * insertionSort com list
     * 
     * @param <T> implementa comparable
     * @param values list para sort
     */
    public static <T extends Comparable<T>> void insertionSort(List<T> values) {
        int i, j;
        T newValue;
        for (i = 1; i < values.size(); i++) {
            newValue = values.get(i);
            j = i;
            while (j > 0 && values.get(j - 1).compareTo(newValue) > 0) {
                values.set(j, values.get(j - 1));
                j--;
            }
            values.set(j, newValue);
        }
    }

    /**
     * mergeSort com array
     * 
     * @param <T> implementa comparable
     * @param values array para sort
     */
    public static <T extends Comparable<T>> void mergeSort(T[] values) {
        if (values.length > 1) {
            int q = values.length / 2;

            T[] leftArray = Arrays.copyOfRange(values, 0, q);
            T[] rightArray = Arrays.copyOfRange(values, q, values.length);

            mergeSort(leftArray);
            mergeSort(rightArray);

            merge(values, leftArray, rightArray);
        }
    }

    /**
     * junta 2 arrays, usado por mergeSort
     * 
     * @param <T> implementa comparable
     * @param values array para sort
     * @param left array da esquerda
     * @param right array da direita
     */
    private static <T extends Comparable<T>> void merge(T[] array, T[] left, T[] right) {
        int totElem = left.length + right.length;
        int i, li, ri;
        i = li = ri = 0;

        while (i < totElem) {
            if ((li < left.length) && (ri < right.length)) {
                if (left[li].compareTo(right[ri]) < 0) {
                    array[i++] = left[li++];
                } else {
                    array[i++] = right[ri++];
                }
            } else {
                if (li >= left.length) {
                    while (ri < right.length) {
                        array[i++] = right[ri++];
                    }
                }
                if (ri >= right.length) {
                    while (li < left.length) {
                        array[i++] = left[li++];
                    }
                }
            }
        }
    }

    /**
     * mergeSort com list 
     * 
     * @param <T> implementa comparable
     * @param values list para sort
     */
    public static <T extends Comparable<T>> void mergeSort(List<T> values) {
        if (values.size() > 1) {
            int q = values.size() / 2;

            List<T> leftArray = new ArrayList<>(values).subList(0, q);
            List<T> rightArray = new ArrayList<>(values).subList(q, values.size());

            mergeSort(leftArray);
            mergeSort(rightArray);

            merge(values, leftArray, rightArray);
        }
    }

    /**
     * junta 2 listas, usado por mergeSort
     * 
     * @param <T> implementa comparable
     * @param values list para sort
     * @param left list da esquerda
     * @param right list da direita
     */
    private static <T extends Comparable<T>> void merge(List<T> array, List<T> left, List<T> right) {
        int totElem = left.size() + right.size();
        int i, li, ri;
        i = li = ri = 0;

        while (i < totElem) {
            if ((li < left.size()) && (ri < right.size())) {
                if (left.get(li).compareTo(right.get(ri)) < 0) {
                    array.set(i++, left.get(li++));
                } else {
                    array.set(i++, right.get(ri++));
                }
            } else {
                if (li >= left.size()) {
                    while (ri < right.size()) {
                        array.set(i++, right.get(ri++));
                    }
                }
                if (ri >= right.size()) {
                    while (li < left.size()) {
                        array.set(i++, left.get(li++));
                    }
                }
            }
        }
    }

    /**
     * quickSort com array
     * 
     * @param <T> implementa comparable
     * @param values array para sort
     * @param left index da esquerda
     * @param right index da direita
     */
    public static <T extends Comparable<T>> void quickSort(T[] values, int left, int right) {
        int index = partition(values, left, right);
        if (left < index - 1) {
            quickSort(values, left, index - 1);
        }
        if (index < right) {
            quickSort(values, index, right);
        }
    }

    /**
     * separa 2 arrays, usado por quickSort
     * 
     * @param <T> implementa comparable
     * @param values array para sort
     * @param left index da esquerda
     * @param right index da direita
     * @return novo index
     */
    private static <T extends Comparable<T>> int partition(T[] values, int left, int right) {
        int i = left, j = right;
        T tmp;
        T pivot = values[(left + right) / 2];

        while (i <= j) {
            while (values[i].compareTo(pivot) < 0) {
                i++;
            }
            while (values[j].compareTo(pivot) > 0) {
                j--;
            }
            if (i <= j) {
                tmp = values[i];
                values[i] = values[j];
                values[j] = tmp;
                i++;
                j--;
            }
        }

        return i;
    }

    /**
     * quick sort com lists
     * 
     * @param <T> implementa comparable
     * @param values array para sort
     * @param left index da esquerda
     * @param right index da direita
     */
    public static <T extends Comparable<T>> void quickSort(List<T> values, int left, int right) {
        int index = partition(values, left, right);
        if (left < index - 1) {
            quickSort(values, left, index - 1);
        }
        if (index < right) {
            quickSort(values, index, right);
        }
    }

    /**
     * separa 2 lists, usado por quickSort
     * 
     * @param <T> implementa comparable
     * @param values array para sort
     * @param left index da esquerda
     * @param right index da direita
     * @return novo index
     */
    private static <T extends Comparable<T>> int partition(List<T> values, int left, int right) {
        int i = left, j = right;
        T tmp;
        T pivot = values.get((left + right) / 2);

        while (i <= j) {
            while (values.get(i).compareTo(pivot) < 0) {
                i++;
            }
            while (values.get(j).compareTo(pivot) > 0) {
                j--;
            }
            if (i <= j) {
                tmp = values.get(i);
                values.set(i, values.get(j));
                values.set(j, tmp);
                i++;
                j--;
            }
        }

        return i;
    }
}
