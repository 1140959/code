package graphexamples;

import graphbase.Edge;
import graphbase.Vertex;
import graphbase.Graph;
import static graphbase.GraphAlgorithms.DepthFirstSearch;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 *
 * @author DEI-ESINF
 */
public class AirportNet {

    private Graph<String, Integer> airport;

    public AirportNet() {
        this.airport = new Graph<>(true);
    }

    public void addAirport(String a) {
        airport.insertVertex(a);
    }

    public void addRoute(String a1, String a2, double miles, Integer npasseng) {
        airport.insertEdge(a1, a2, npasseng, miles);
    }

    public int keyAirport(String airp) {
        return airport.getVertex(airp).getKey();
    }

    public String airportbyKey(int key) {
        return airport.getVertex(key).getElement();
    }

    public Integer trafficAirports(String airp1, String airp2) {

        Vertex<String, Integer> vairp1 = airport.getVertex(airp1);
        Vertex<String, Integer> vairp2 = airport.getVertex(airp2);

        if (vairp1 == null || vairp2 == null) {
            return null;
        }

        Edge<String, Integer> edge = airport.getEdge(vairp1, vairp2);
        if (edge == null) {
            return null;
        }

        return edge.getElement();
    }

    public Double milesAirports(String airp1, String airp2) {

        Vertex<String, Integer> vairp1 = airport.getVertex(airp1);
        Vertex<String, Integer> vairp2 = airport.getVertex(airp2);

        if (vairp1 == null || vairp2 == null) {
            return null;
        }

        Edge<String, Integer> edge = airport.getEdge(vairp1, vairp2);
        if (edge == null) {
            return null;
        }

        return edge.getWeight();
    }

    public String nroutesAirport() {

        List<Integer> routesAirp = new ArrayList<>();

        for (Vertex<String, Integer> vertex : airport.vertices()) {
            int grau = airport.outDegree(vertex) + airport.inDegree(vertex);
            routesAirp.add(grau);
        }
        return routesAirp.toString();
    }

    public String AirpMaxMiles() {

        List<String> maxMilesAirports = new ArrayList<>();
        double maxmiles = 0;

        for (Vertex<String, Integer> vertex : airport.vertices()) {
            for (Edge<String, Integer> edge : airport.outgoingEdges(vertex)) {
                if (edge.getWeight() >= maxmiles) {
                    if (edge.getWeight() > maxmiles) {
                        maxMilesAirports.clear();
                    }
                    maxmiles = edge.getWeight();

                    Vertex<String, Integer> vDest = airport.opposite(vertex, edge);
                    maxMilesAirports.add(vertex.getElement());
                    maxMilesAirports.add(vDest.getElement());
                }
            }
        }
        return maxMilesAirports.toString();
    }

    public Boolean ConnectAirports(String airp1, String airp2) {

        Deque<String> qairps = DepthFirstSearch(airport, airp1);

        for (String airp : qairps) {
            if (airp.equals(airp2)) {
                return true;
            }
        }

        return false;
    }

}
