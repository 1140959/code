package graphexamples;

import graphbase.Graph;
import graphbase.Vertex;
import static graphbase.GraphAlgorithms.*;
import java.util.ArrayList;
import java.util.Deque;

/**
 *
 * DEI-ESINF
 */
public class Museum {

    //------------ Static nested Room class ------------
    public static class Room {

        private Integer number;
        private Double time;

        public Room(Integer n, Double t) {
            number = n;
            time = t;
        }

        public Integer getNumber() {
            return number;
        }

        public void setNumber(Integer n) {
            number = n;
        }

        Double getTime() {
            return time;
        }

        public void setTime(Double t) {
            time = t;
        }

        @Override
        public boolean equals(Object otherObj) {
            if (this == otherObj) {
                return true;
            }
            if (otherObj == null || this.getClass() != otherObj.getClass()) {
                return false;
            }
            Room otherRoom = (Room) otherObj;

            return this.number == otherRoom.number;
        }

    }
    //------------ end of Static nested Room class ------------

    private Graph<Room, Integer> exhibition;

    public Museum() {
        this.exhibition = new Graph<>(false);
    }

    public void addtwoConnectedRooms(Room r1, Room r2) {
        exhibition.insertEdge(r1, r2, null, 0);
    }

    public double timevisitAllrooms() {
        double tottime = 0;
        for (Vertex<Room, Integer> vertex : exhibition.vertices()) {
            tottime += vertex.getElement().getTime();
        }
        return tottime;
    }

    public double timeOnevisit(Deque<Room> visit) {

        double timevisit = 0;
        for (Room r : visit) {
            timevisit += r.getTime();
        }
        return timevisit;
    }

    public ArrayList<Deque<Room>> visitwithLimitedtime(Room r1, Room r2, double time) {

        ArrayList<Deque<Room>> allvisits = allPaths(this.exhibition, r1, r2);
        ArrayList<Deque<Room>> visits = new ArrayList<>();

        for (Deque<Room> pathExhib : allvisits) {
            double timevisit = timeOnevisit(pathExhib);
            if (timevisit <= time) {
                visits.add(pathExhib);
            }
        }
        return visits;
    }

    public Deque<Room> visitwithAllrooms(Room r) {
        for (Vertex<Room, Integer> vertex : exhibition.vertices()) {
            Room rdst = vertex.getElement();
            Deque<Room> pathExhib = null;
            ArrayList<Deque<Room>> allpaths = allPaths(this.exhibition, r, rdst);
            if (checkAllRooms(allpaths, pathExhib)) {
                return pathExhib;
            }
        }
        return null;
    }

    private boolean checkAllRooms(ArrayList<Deque<Room>> allpaths, Deque<Room> pathExhib) {
        for (Deque<Room> path : allpaths) {
            if (path.size() == this.exhibition.numVertices()) {
                pathExhib = path;
                return true;
            }
        }
        return false;
    }
}
